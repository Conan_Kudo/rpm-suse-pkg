#
# spec file for package rpm-python
#
# Copyright (c) 2017 SUSE LINUX GmbH, Nuernberg, Germany.
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.

# Please submit bugfixes or comments via http://bugs.opensuse.org/
#


Name:           rpm-python
Version:        4.13.0
Release:        0
#!BuildIgnore:  rpmlint-Factory
Summary:        Python Bindings for Manipulating RPM Packages
License:        GPL-2.0+
Group:          System/Packages
Source99:       rpm.spec
BuildRequires:  file-devel
BuildRequires:  libacl-devel
BuildRequires:  libbz2-devel
BuildRequires:  libcap-devel
BuildRequires:  libelf-devel
BuildRequires:  libselinux-devel
BuildRequires:  libsemanage-devel
BuildRequires:  libtool
BuildRequires:  lua-devel
BuildRequires:  ncurses-devel
BuildRequires:  popt-devel
BuildRequires:  python-devel
BuildRequires:  xz-devel
BuildRequires:  zlib-devel
Requires:       rpm = %{version}
%py_requires
%{expand:%(sed -n -e '/^Source:/,/^BuildRoot:/p' <%_sourcedir/rpm.spec)}
%global with_python 2

%description
The rpm-python package contains a module that permits applications
written in the Python programming language to use the interface
supplied by RPM Package Manager libraries.

This package should be installed if you want to develop Python programs
that will manipulate RPM packages and databases.

%prep
%{expand:%(sed -n -e '/^%%prep/,/^%%install/p' <%_sourcedir/rpm.spec | sed -e '1d' -e '$d')}

%install
mkdir -p %{buildroot}%{_prefix}/lib
# only installing in python/ does not work because rpm links against
# installed libs at install time
%make_install
find %{buildroot} -not -type d -and -not -path %{buildroot}%{_libdir}/python%{py_ver}/site-packages/rpm/\* -print0 | xargs -0 rm
pushd %{buildroot}/%py_sitedir/rpm
rm -f _rpm*.a _rpm*.la
python %py_libdir/py_compile.py *.py
python -O %py_libdir/py_compile.py *.py
popd

%files
%defattr(-,root,root)
%{_libdir}/python*/*/*

%changelog
