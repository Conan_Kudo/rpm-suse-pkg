#
# spec file for package python3-rpm
#
# Copyright (c) 2017 SUSE LINUX GmbH, Nuernberg, Germany.
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.

# Please submit bugfixes or comments via http://bugs.opensuse.org/
#


Name:           python3-rpm
Version:        4.13.0
Release:        0
Summary:        Python Bindings for Manipulating RPM Packages
License:        GPL-2.0+
Group:          System/Packages
Source99:       rpm.spec
BuildRequires:  file-devel
BuildRequires:  libacl-devel
BuildRequires:  libbz2-devel
BuildRequires:  libcap-devel
BuildRequires:  libelf-devel
BuildRequires:  libselinux-devel
BuildRequires:  libsemanage-devel
BuildRequires:  libtool
BuildRequires:  lua-devel
BuildRequires:  ncurses-devel
BuildRequires:  popt-devel
BuildRequires:  python3-devel
BuildRequires:  xz-devel
BuildRequires:  zlib-devel
Requires:       rpm = %{version}
%{expand:%(sed -n -e '/^Source:/,/^BuildRoot:/p' <%_sourcedir/rpm.spec)}
%global with_python 3

%description
The python3-rpm package contains a module that permits applications
written in the Python programming language to use the interface
supplied by RPM Package Manager libraries.

This package should be installed if you want to develop Python programs
that will manipulate RPM packages and databases.

%prep
%{expand:%(sed -n -e '/^%%prep/,/^%%install/p' <%_sourcedir/rpm.spec | sed -e '1d' -e '$d')}

%install
mkdir -p %{buildroot}%{_prefix}/lib
# only installing in python/ does not work because rpm links against
# installed libs at install time
%make_install
find %{buildroot} -not -type d -and -not -path %{buildroot}%{_libdir}/python3*/site-packages/rpm/\* -print0 | xargs -0 rm
pushd %{buildroot}/%{_libdir}/python3*/site-packages/rpm
rm -f _rpm*.la _rpm*.a
python3 %{_libdir}/python3*/py_compile.py *.py
python3 -O %{_libdir}/python3*/py_compile.py *.py
popd

%files
%defattr(-,root,root)
%{_libdir}/python*/*/*

%changelog
